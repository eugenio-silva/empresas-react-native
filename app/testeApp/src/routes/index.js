import React from 'react';

import {AppStack} from './app.route';

export const Routes = () => {
  return <AppStack />;
};
