import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {Dashboard} from '../screens/Dashboard/index';
import {Sign} from '../screens/Sign/index';

const Stack = createStackNavigator();

const headerDefaultOptions = {
  headerTransparent: true,
  headerTintColor: '#fff',
};

export const AppStack = () => (
  <Stack.Navigator initialRouteName="Sign">
    <Stack.Screen
      name="Sign"
      component={Sign}
      options={{
        ...headerDefaultOptions,
        title: 'Tela Login',
      }}
    />
    <Stack.Screen
      name="Dashboard"
      component={Dashboard}
      options={{
        ...headerDefaultOptions,
        headerLeft: null,
        title: 'Dashboard',
      }}
    />
  </Stack.Navigator>
);
