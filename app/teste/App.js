import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';

import {store} from './src/store';
import {Routes} from './src/routes';
import {NavigationContainer} from '@react-navigation/native';
import {Provider as StoreProvider} from 'react-redux';

import {
  SafeAreaProvider,
  initialWindowMetrics,
} from 'react-native-safe-area-context';

export const App = () => {
  return (
    <StoreProvider store={store}>
      <SafeAreaProvider initialMetrics={initialWindowMetrics}>
        <NavigationContainer>
          <StatusBar
            translucent
            backgroundColor="transparent"
            showHideTransition="fade"
          />
          <Routes />
        </NavigationContainer>
      </SafeAreaProvider>
    </StoreProvider>
  );
};
