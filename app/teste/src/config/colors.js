export const colors = {
  backgroundPrimary: '#141414',
  backgroundSecundary: '#1b1b1b',
  black: '#000',

  primary: '#ff9000',
  white: '#d5d5d5',

  error: '#cc0000',
};
