import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {Dashboard} from '../screens/Dashboard/index';
import {Login} from '../screens/Login/index';
import {EnterpriseDetails} from '../screens/EnterpriseDetails/index';

const Stack = createStackNavigator();

const headerDefaultOptions = {
  headerTransparent: true,
  headerTintColor: '#fff',
};

export const AppStack = () => (
  <Stack.Navigator initialRouteName="Login">
    <Stack.Screen
      name="Login"
      component={Login}
      options={{
        ...headerDefaultOptions,
        title: '',
      }}
    />
    <Stack.Screen
      name="Dashboard"
      component={Dashboard}
      options={{
        ...headerDefaultOptions,
        headerLeft: null,
        title: 'Dashboard',
      }}
    />
    <Stack.Screen
      name="EnterpriseDetails"
      component={EnterpriseDetails}
      options={{
        ...headerDefaultOptions,
        title: '',
      }}
    />
  </Stack.Navigator>
);
