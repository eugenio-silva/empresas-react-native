import React, {useState} from 'react';
import {Alert, Text} from 'react-native';

import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {setUser} from '../../store/User/User.action';
import {api} from '../../services/api';

import {
  Container,
  TextButton,
  Content,
  InputWrapper,
  ButtonWrapper,
} from './Login.styled';
import {Input} from '../../components/Input';
import {Button} from '../../components/Button';
import {Logo} from '../../components/Logo';
import {colors} from '../../config/colors';

export const Login = () => {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [value, setValue] = useState(user);

  const [isError, setIsError] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [teste, setTeste] = useState(false);

  const getAuth = async () => {
    try {
      errorValidation(email);
      const {
        headers: {'access-token': accessToken, client, uid},
      } = await api.post('/users/auth/sign_in', {
        email: email,
        password: password,
      });
      setValue({accessToken, client, uid});
      setTeste(true);
    } catch (error) {
      Alert.alert('Ops', 'Error na conexão!');
    }
  };

  const handleClick = () => {
    getAuth();

    dispatch(setUser(value));
    navigation.navigate('Dashboard');
  };

  const errorValidation = email => {
    if (!email) return setIsError(true);
  };

  return (
    <Container>
      <Logo />
      <Content>
        {!teste && (
          <>
            <InputWrapper>
              <Input
                placeholder="E-mail"
                value={email}
                onChangeText={text => setEmail(text)}
              />
              {isError && (
                <Text style={{color: colors.error}}>* Campo Obrigatório</Text>
              )}
              <Input
                placeholder="Senha"
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry
              />
              {isError && (
                <Text style={{color: colors.error}}>* Campo Obrigatório</Text>
              )}
            </InputWrapper>
            <ButtonWrapper>
              <Button onPress={getAuth}>
                <TextButton>Login</TextButton>
              </Button>
            </ButtonWrapper>
          </>
        )}

        {teste && (
          <ButtonWrapper>
            <Button onPress={handleClick}>
              <TextButton>Avançar</TextButton>
            </Button>
          </ButtonWrapper>
        )}
      </Content>
    </Container>
  );
};
