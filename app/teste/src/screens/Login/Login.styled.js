import styled from 'styled-components/native';
import {colors} from '../../config/colors';

export const Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  align-items: center;
  justify-content: space-around;
  padding: 5%;
`;

export const Content = styled.View`
  width: 100%;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 18px;
`;

export const InputWrapper = styled.View`
  height: 170px;
  justify-content: space-between;
`;

export const ButtonWrapper = styled.View`
  margin-top: 5%;
`;
