import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  Alert,
  Text,
  FlatList,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';
// import {setUser} from '../../store/User/User.action';
import {api} from '../../services/api';

import {
  Container,
  ContentList,
  CardWrapper,
  TextCard,
  TextButton,
} from './Dashboar.styled';
import {Button} from '../../components/Button';

export const Dashboard = () => {
  const user = useSelector(state => state.user);
  const [userAuth, setUserAuth] = useState(user);
  const [enterprisesList, setEnterprisesList] = useState([]);
  const [loading, setLoading] = useState(false);

  const navigation = useNavigation();

  const handleClick = async () => {
    try {
      const {data} = await api.get('/enterprises', {
        headers: {
          client: userAuth.client,
          uid: userAuth.uid,
          'access-token': userAuth.accessToken,
        },
      });
      console.log(enterprisesList);
      setLoading(false);
      setEnterprisesList(data.enterprises);
    } catch (error) {
      Alert.alert('Ops!', 'Falha na requisição!');
    }
  };

  const Item = ({data}) => (
    <CardWrapper>
      <TextCard>{data.enterprise_name}</TextCard>
      <TouchableOpacity
        onPress={() => navigation.navigate('EnterpriseDetails')}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <Text style={{color: '#ff9900', marginRight: 5}}>Detalhes</Text>
        <Text style={{color: '#ff9900', fontSize: 20}}>&gt;</Text>
      </TouchableOpacity>
    </CardWrapper>
  );

  return (
    <Container>
      {loading ? (
        <ContentList>
          <ActivityIndicator
            size="large"
            color="#ff9900"
            visible={loading}
            textStyle={{color: '#fff'}}
          />
        </ContentList>
      ) : (
        <>
          <ContentList>
            <FlatList
              data={enterprisesList}
              renderItem={({item}) => <Item data={item} />}
              keyExtractor={item => item.id}
            />
          </ContentList>
        </>
      )}

      <Button onPress={handleClick}>
        <TextButton>Buscar Empresas</TextButton>
      </Button>
    </Container>
  );
};
