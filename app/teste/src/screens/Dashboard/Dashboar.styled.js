import styled from 'styled-components/native';
import {colors} from '../../config/colors';

export const Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  padding: 5% 5%;
  justify-content: space-between;
  align-items: center;
`;

export const ContentList = styled.View`
  width: 90%;
  height: 60%;
  margin-top: 20%;
`;

export const CardWrapper = styled.View`
  margin-bottom: 10%;
  background-color: ${colors.backgroundSecundary};
  height: 80px;
  padding: 5%;
  justify-content: space-between;
`;

export const TextCard = styled.Text`
  color: ${colors.white};
`;

export const ImageEnterprise = styled.Image`
  height: 60px;
  width: 100%;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 18px;
`;
