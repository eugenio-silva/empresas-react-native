import {color} from 'react-native-reanimated';
import styled from 'styled-components/native';
import {colors} from '../../config/colors';

export const Container = styled.View`
  flex: 1;
  width: 100%;
  background-color: ${colors.backgroundPrimary};
  padding: 5% 5%;
  align-items: center;
  justify-content: center;
`;

export const Content = styled.View`
  width: 100%;
  height: 350px;
  background-color: ${colors.backgroundSecundary};
  padding: 5%;
`;

export const EnterpriseName = styled.Text`
  color: ${colors.primary};
  font-size: 18px;
`;

export const EnterpriseDescription = styled.Text`
  color: ${colors.white};
  margin-top: 5%;
`;

export const CityText = styled.Text`
  color: #fff;
`;

export const CountryText = styled.Text`
  color: #fff;
`;

export const Wrapper = styled.View`
  justify-content: space-around;
  align-items: flex-start;
  height: 290px;
`;

export const WrapperButton = styled.View`
  margin-top: 5%;
  width: 100%;
`;
