# Teste Ioasys

## Para testar o projeto

- CLone o Repositóio

- Cole o comando abaixo no terminal, no diretório de escolha:

```
git clone https://eugenio-silva@bitbucket.org/eugenio-silva/empresas-react-native.git
```

- Entre no diretório:

```
cd empresas-react-native
cd app
cd teste
```

- Execute o comando abaixo:

```
npm install
```

- Execute o comando abaixo para executar o projeto no simulador

```
npm android ou npm ios
```

---

## Bibliotecas usadas:

- react-redux;
- react-navigation;
- axios;
- styled-components;

---

## Agradecimentos

Gostaria de agradecer pela oportunidade do teste, apesar de não ter terminado devido alguns problemas, foi de muito aprendizado.
O teste foi muito bem aplicado e justo.
