import React, {useState} from 'react';
import {Text, Alert} from 'react-native';
import {useSelector} from 'react-redux';

import {api} from '../../services/api';

import {
  Container,
  Content,
  EnterpriseName,
  EnterpriseDescription,
  CityText,
  CountryText,
  Wrapper,
  WrapperButton,
} from './EnterpriseDetails.styled';
import {Button} from '../../components/Button/index';

export const EnterpriseDetails = () => {
  const id = 7;
  const user = useSelector(state => state.user);
  const [enterprisesList, setEnterprisesList] = useState([]);
  const [userAuth, setUserAuth] = useState(user);

  const handleClick = async () => {
    try {
      const {data} = await api.get(`/enterprises/${id}`, {
        headers: {
          client: userAuth.client,
          uid: userAuth.uid,
          'access-token': userAuth.accessToken,
        },
      });
      console.log(enterprisesList);
      setEnterprisesList(data.enterprise);
    } catch (error) {
      Alert.alert('Ops!', 'Falha na requisição!');
    }
  };

  return (
    <>
      <Container>
        <Content>
          <EnterpriseName>{enterprisesList.enterprise_name}</EnterpriseName>

          <Wrapper>
            <EnterpriseDescription>
              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                Description:{' '}
              </Text>
              {enterprisesList.description}
            </EnterpriseDescription>

            <CityText>
              <Text style={{fontWeight: 'bold', fontSize: 16}}>City:</Text>{' '}
              {enterprisesList.city}
            </CityText>
            <CountryText>
              {' '}
              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                Country:
              </Text>{' '}
              {enterprisesList.country}
            </CountryText>

            <Text style={{color: '#FFF'}}>{enterprisesList.share_price}</Text>
            <Text style={{color: '#FFF'}}>{enterprisesList.shares}</Text>
          </Wrapper>
        </Content>
        <WrapperButton>
          <Button style={{backgroundColor: '#cc0000'}} onPress={handleClick}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>
              Vizualizar Detalhes
            </Text>
          </Button>
        </WrapperButton>
      </Container>
    </>
  );
};
