import styled from 'styled-components/native';
import {colors} from '../../config/colors';

export const Container = styled.View`
  width: 100%;
  height: 70px;
`;

export const InputWrapper = styled.TextInput`
  background-color: ${colors.backgroundSecundary};
  padding: 5%;
  color: ${colors.white};
`;
