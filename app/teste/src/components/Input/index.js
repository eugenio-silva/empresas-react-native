import React from 'react';
import {Container, InputWrapper} from './Input.styled';

export const Input = ({placeholder, secureTextEntry, onChangeText}) => {
  return (
    <Container>
      <InputWrapper
        placeholder={placeholder}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        placeholderTextColor="#d5d5d5"
      />
    </Container>
  );
};
