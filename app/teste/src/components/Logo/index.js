import React from 'react';
import {Text} from 'react-native';

import {Container} from './Logo.styles';

export const Logo = () => {
  return (
    <Container>
      <Text style={{color: '#ff9000', fontSize: 50}}>.</Text>
      <Text
        style={{
          color: '#fdfdfd',
          fontSize: 35,
          fontWeight: 'bold',
          letterSpacing: 5,
        }}>
        teste
      </Text>
    </Container>
  );
};
