import React from 'react';
import {MyButton} from './Button.styled';

export const Button = ({color, onPress, title, children}) => {
  return (
    <MyButton title={title} color={color} onPress={onPress}>
      {children}
    </MyButton>
  );
};
