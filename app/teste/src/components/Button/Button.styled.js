import styled from 'styled-components/native';
import {colors} from '../../config/colors';

export const MyButton = styled.TouchableOpacity`
  width: 100%;
  height: 60px;

  background-color: ${colors.primary};

  align-items: center;
  justify-content: center;

  border-radius: 5px;
`;
