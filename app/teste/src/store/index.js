import createStore from './createStore';
import {combinedReducers} from './modules/rootReducer';

const middlewares = [];

const store = createStore(combinedReducers, middlewares);

export {store};
