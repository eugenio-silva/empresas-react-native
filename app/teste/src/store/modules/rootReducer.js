import {combineReducers} from 'redux';

import userReducer from '../User/User.reducer';

export const combinedReducers = combineReducers({
  user: userReducer,
});
