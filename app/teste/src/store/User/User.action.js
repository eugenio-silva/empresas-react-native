export const setUser = value => ({
  type: 'SET_USER',
  payload: value,
});
