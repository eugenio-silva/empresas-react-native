const INITIAL_STATE = {
  accessToken: null,
  client: null,
  uid: null,
};

const UserReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_USER':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};

export default UserReducer;
